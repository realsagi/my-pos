package shoponline

class Newstock {

	String barcode
	String nameproduct
	String modeproduct
	Double pricetoon
	Double priceperunit
	Double pricebig
	int countproduct
	Date productimport
	Date productexport
	String detail

    static constraints = {
    	barcode unique: true
    }
}
